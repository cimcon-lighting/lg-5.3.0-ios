//
//  SLCParent.swift
//  SLC Admin
//
//  Created by Apple on 03/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class LGParent: UIViewController {
    
    var serviceHandler : ServiceHandler!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        serviceHandler = ServiceHandler.sharedInstance()
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
    }
    
    func addLeftBarButton(imgLeftbarButton: String) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let leftButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: imgLeftbarButton), style: UIBarButtonItem.Style.done, target:self, action:#selector(leftButtonClick))
        self.navigationItem.leftBarButtonItem = leftButton
    }
    
    func addRightBarButton(imgRightbarButton: String) {
        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.navigationBar.tintColor = UIColor.white
        let rightButton: UIBarButtonItem = UIBarButtonItem(image: UIImage.init(named: imgRightbarButton), style: UIBarButtonItem.Style.done, target:self, action:#selector(rightButtonClick))
        self.navigationItem.rightBarButtonItem = rightButton
    }
    
    @objc func leftButtonClick() {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func showUpgardeAlert() {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "UpragdeID")
        //storyboard.modalPresentationStyle = .fullScreen
        self.present(storyboard, animated: true, completion: nil)
        
        /*let alert = UIAlertController(title: APPNAME, message: appDelegate.strMsgFrmAPI, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
        alert.addAction(UIAlertAction(title: "Upgarde".localized(), style: .default, handler: { action in
            self.dismiss(animated: true) {
                let subject          = "Upgrade to Paid version"
                let body            = ""
                let encodedParams   = "subject=\(subject)&body=\(body)".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
                
                if let url = URL(string: "mailto:Mobile.support@cimconlighting.com?\(encodedParams!)") {
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(url)
                    } else {
                        UIApplication.shared.openURL(url)
                    }
                }
            }
        }))
        appDelegate.window?.rootViewController!.present(alert, animated: true, completion: nil)*/
    }
    
    @objc func rightButtonClick() {
        
    }
}
