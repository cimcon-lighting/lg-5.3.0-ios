//
//  AssignedSLCVC.swift
//  Lighting Gale
//
//  Created by Apple on 01/10/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import FirebaseAnalytics

class CellAssigned : UITableViewCell {
    @IBOutlet var imgStatus         : UIImageView!
    
    @IBOutlet var lblSLCNm          : UILabel!
    @IBOutlet var lblSLCId          : UILabel!
    @IBOutlet var lblMACAdd         : UILabel!
    @IBOutlet var lblLast           : UILabel!
    
    @IBOutlet var lblHeadSLCNm      : UILabel!
    @IBOutlet var lblHeadSLCId      : UILabel!
    @IBOutlet var lblHeadMACAdd     : UILabel!
    @IBOutlet var lblHeadLast       : UILabel!
}

class AssignedSLCVC: LGParent {
    
    @IBOutlet var constVWHeight : NSLayoutConstraint!
    
    var isFilterOpen        : Bool! = false
    
    var strGatewayID        : String!
    
    @IBOutlet var lblMsg    : UILabel!
    
    @IBOutlet var tblView   : UITableView!
    
    @IBOutlet var txtSLCID     : UITextField!
    @IBOutlet var txtSLCName   : UITextField!
    @IBOutlet var txtMACId     : UITextField!
    
    var totalCount          : Int! = 1
    var pageCount           : Int! = 1
    var selectedTAG         : Int! = 1
    
    @IBOutlet var btnSearch     : UIButton!
    @IBOutlet var btnClear      : UIButton!
    
    var refreshControl      : UIRefreshControl!
    
    var arrTblData          : [AnyObject] = []
    var arrGateway          : [AnyObject] = []
    var arrSearchData       : [String]!   = []
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        constVWHeight .constant = 0
        addLeftBarButton(imgLeftbarButton: "back")
        addRightBarButton(imgRightbarButton: "Search")
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        lblMsg.isHidden = true
        
        tblView.tableFooterView = UIView()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        btnSearch.setTitle("SEARCH".localized(), for: .normal)
        btnClear.setTitle("CLEAR".localized(), for: .normal)
        
        let selectLocalize = "Select".localized()
        
        txtSLCName.placeholder = "SLC Name".localized()
        
        if txtSLCID.text == "Select" || txtSLCID.text == "Selecionar" || txtSLCID.text == "Seleccionar" {
            txtSLCID.text         = selectLocalize
        }
        
        if txtSLCName.text == "Select" || txtSLCName.text == "Selecionar" || txtSLCName.text == "Seleccionar" {
            txtSLCName.text         = selectLocalize
        }
        
        if txtMACId.text == "Select" || txtMACId.text == "Selecionar" || txtMACId.text == "Seleccionar" {
            txtMACId.text         = selectLocalize
        }
        
        self.navigationItem.title = "Assigned SLC(s)".localized()
        
        getDataFrmServer(isLoader: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("assignedSLC", screenClass: "assignedSLC")
    }
    
    
    @objc func refresh(sender:AnyObject) {
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        
        txtSLCID.text       = "Select".localized()
        txtSLCName.text     = "Select".localized()
        txtMACId.text       = "Select".localized()
        
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataFrmServer(isLoader: true)
        })
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchIdenifier" {
            let searchVC : SearchViewController = segue.destination as! SearchViewController
            searchVC.searchDelegate = self
            searchVC.strPlasHolder  = (sender as! String)
            searchVC.arrTblData     = arrSearchData
            searchVC.currentTag     = selectedTAG
            searchVC.arrMainData    = arrGateway
            searchVC.strGatewayID   = strGatewayID
            searchVC.isFrmAssignedSLC = true
        }
    }
    
    //MARK:- getDataFrmServer
    func getDataFrmServer(isLoader : Bool) {
        
        if arrTblData.count != totalCount {
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            dictHeader[PAGE_NO]         = String(pageCount)
            dictHeader[PAGE_SIZE]       = "10"
            
            let strSelectTx = "Select".localized()
            
            var dictData : [String : AnyObject] = [:]
            dictData["SLCNO"]          = txtSLCID.text   == strSelectTx ? "" as AnyObject : txtSLCID.text        as AnyObject
            dictData["SLCName"]        = txtSLCName.text == strSelectTx ? "" as AnyObject : txtSLCName.text      as AnyObject
            dictData["uid"]            = txtMACId.text   == strSelectTx ? "" as AnyObject : txtMACId.text        as AnyObject
            dictData["GatewayID"]      = strGatewayID             as AnyObject
            
            ProfileServices().getAssignedSLCs(parameters: dictData, headerParams: dictHeader, showLoader: isLoader) { (responseData, isSuccess) in
                if isSuccess {
                    if self.arrTblData.count != self.totalCount {
                        let arrTmp : [AnyObject] = responseData["listSLC"]  as! [AnyObject]
                        self.arrTblData.append(contentsOf: arrTmp)
                        self.totalCount = (responseData[TOTAL_RECORDS] as! Int)
                    }
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.pageCount += 1
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                } else {
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                }
            }
        } else {
            self.refreshControl.endRefreshing()
            if self.arrTblData.count == 0 {
                self.lblMsg.text = "No data Found".localized()
                self.lblMsg.isHidden = false
            }
        }
    }
    
    override func rightButtonClick() {
        if !isFilterOpen {
            isFilterOpen = true
            constVWHeight .constant = 153
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        } else {
            isFilterOpen = false
            constVWHeight .constant = 0
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func searchBtnClick (sender : UIButton) {
        
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_searchAssignedSlc"
        ])
        txtSLCName.resignFirstResponder()
        txtMACId.resignFirstResponder()
        pageCount           = 1
        totalCount          = 1
        arrTblData.removeAll()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataFrmServer(isLoader: true)
        })
    }
    
    @IBAction func clearBtn_Click(sender : UIButton) {
        txtSLCName.resignFirstResponder()
        txtMACId.resignFirstResponder()
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        txtSLCID.text   = "Select".localized()
        txtSLCName.text = "Select".localized()
        txtMACId.text   = "Select".localized()
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataFrmServer(isLoader: true)
        })
    }
    
    //MARK:- getSLCs
    func getSLCs() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        dictHeader[SLC_NO]          = ""
        dictHeader["Gatewayid"]     = strGatewayID
        dictHeader["lamptypeid"]    = "0"
        dictHeader["NodeType"]      = ""
        
        ProfileServices().getSLCList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                var arrTmp : [String] = []
                self.arrGateway = responseData[DATA] as! [AnyObject]
                for dict in self.arrGateway {
                    arrTmp.append(dict["value"] as! String)
                }
                self.arrSearchData = arrTmp
                self.performSegue(withIdentifier: "SearchIdenifier", sender: "SLC#")
            }
        }
    }
    
}
extension AssignedSLCVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTblData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellAssigned = tblView.dequeueReusableCell(withIdentifier: "CellAssigned") as! CellAssigned
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        cell.lblHeadLast.text   = "Last Communicated On".localized()
        cell.lblHeadSLCId.text  = "SLC"+" #".localized()
        cell.lblHeadSLCNm.text  = "SLC Name".localized()
        cell.lblHeadMACAdd.text = "UID".localized()
        
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
        cell.lblLast.text   = dictData["lastCommDate"]  as? String
        cell.lblSLCId.text  = dictData["slcNo"]         as? String
        cell.lblSLCNm.text  = dictData["slcName"]       as? String
        cell.lblMACAdd.text = dictData["uid"]           as? String
        
        if indexPath.row == arrTblData.count-1 {
            getDataFrmServer(isLoader: true)
        }
        
        if dictData["statusName"] as! String == "SLC Never communicated" {
            cell.imgStatus.image = UIImage.init(named: "P")
        } else if dictData["statusName"] as! String == "Communcation Faults" {
            cell.imgStatus.image = UIImage.init(named: "cfail")
        } else if dictData["statusName"] as! String == "Lamp Dimmed" {
            cell.imgStatus.image = UIImage.init(named: "dim")
        } else if dictData["statusName"] as! String == "Lamp On" {
            cell.imgStatus.image = UIImage.init(named: "on")
        } else if dictData["statusName"] as! String == "Lamp Off" {
            cell.imgStatus.image = UIImage.init(named: "off")
        } else if dictData["statusName"] as! String == "undefined" {
            cell.imgStatus.image = UIImage.init(named: "")
        }
        
        return cell
    }
}

extension AssignedSLCVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTAG = textField.tag
        if textField == txtSLCID {
            getSLCs()
            return false
        } else if textField == txtSLCName {
            textField.text = ""
            return true
        } else if textField == txtMACId {
            textField.text = ""
            return true
        } else {
            return true
        }
    }
}


extension AssignedSLCVC : SearchVCDelegate {
    func changeSearchParams(_ placeHolder: String, tagTextField: Int) {
        
    }
    
    func changeSearchParamsStatus(_ placeHolder: String, tagTextField: Int, strGroupID : String) {
        if tagTextField == 1 {
            txtSLCID.text = placeHolder
        }
    }
}

