//
//  ProfileViewController.swift
//  SLC Admin
//
//  Created by Apple on 08/01/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import Localize_Swift
import FirebaseAnalytics

class ProfileViewController: LGParent {

    @IBOutlet var txtNm         : UITextField!
    @IBOutlet var txtUName      : UITextField!
    @IBOutlet var txtEmail      : UITextField!
    @IBOutlet var txtLangauge   : UITextField!
    @IBOutlet var txtMaptype    : UITextField!
    
    @IBOutlet var lblVersion    : UILabel!
    
    var arrTblData              : [String]!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.addRightBarButton(imgRightbarButton: "Logoff")
        
        
        self.addLeftBarButton(imgLeftbarButton: "back")

    }
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       localizationSetup()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
           super.viewDidAppear(animated)
           Analytics.setScreenName("profile", screenClass: "profile")
       }
    
    override func rightButtonClick() {
        Localize.resetCurrentLanguageToDefault()
        UserdefaultManager().clearSessions()
        _ = self.navigationController?.navigationController!.popToRootViewController(animated: true)
    }
    
    func localizationSetup() {       
         
               let version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
               lblVersion.text = "Version".localized()+": "+version!
                      
               self.navigationItem.title = "Profile".localized()
               
               arrTblData = ["Apple Satellite".localized(),
                             "Apple Standard".localized(),
                             "Apple Hybrid".localized(),
                             "Google Map Satellite".localized(),
                             "Google Map Standard".localized(),
                             "Google Map Hybrid".localized(),
                             "Open Street Standard".localized()]
               
               //localizationSetup()

               txtUName.placeholder    = "Username".localized()
               txtNm.placeholder       = "Name".localized()
               txtLangauge.placeholder = "Select Language".localized()
               txtEmail.placeholder    = "Phone Number".localized()
        
               txtUName.text   = serviceHandler.objUserModel.strUName
               txtNm.text      = serviceHandler.objUserModel.strFName
               txtEmail.text   = serviceHandler.objUserModel.strPhone
               //txtMaptype.text = arrTblData[appDelegate.strSelectedMap]
               
               let strSelectedlang : String = UserdefaultManager().getPreferenceForkey(APPLANGUAGE) as! String
               
               if strSelectedlang == "pt" {
                   txtLangauge.text = "Portuguese".localized()
               } else if (strSelectedlang == "es") {
                   txtLangauge.text = "Spanish".localized()
               } else {
                   txtLangauge.text = "English".localized()
               }
               
               tabBarController?.viewControllers![0].title = "DASHBOARD".localized()
               tabBarController?.viewControllers![1].title = "STATUS".localized()
               tabBarController?.viewControllers![2].title = "MAP".localized()
               tabBarController?.viewControllers![3].title = "MORE".localized()
    }
 
    //MARK:- ActionMapSheet
    func actionMapSheet() {
        view.endEditing(true)
        
        let actionSheet = UIAlertController(title: "Select Map Type".localized(), message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
    
        for strTitle in arrTblData {
            if appDelegate.strSelectedMap == arrTblData.index(of: strTitle) {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .destructive, handler: { action in
                    self.dismiss(animated: true) {
                    }
                }))
            } else {
                actionSheet.addAction(UIAlertAction(title: strTitle, style: .default, handler: { action in
                    appDelegate.strSelectedMap = self.arrTblData.index(of: strTitle)!
                    self.txtMaptype.text = strTitle
                    self.dismiss(animated: true) {
                    }
                }))
            }
        }
        if UI_USER_INTERFACE_IDIOM() == .pad {
            actionSheet.modalPresentationStyle = .popover
            let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
            popPresenter?.sourceView =  self.view;
            popPresenter?.sourceRect =  self.view.bounds;
        }
        
        present(actionSheet, animated: true)
    }

    //MARK:- actionLanguageSheet
    func actionLanguageSheet() {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_select_lang_type"
        ])
        
        
        view.endEditing(true)
        
        let actionSheet = UIAlertController(title: "Select Language".localized(), message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Cancel".localized(), style: .cancel, handler: { action in
            self.dismiss(animated: true) {
            }
        }))
        
        for strTitle: String in ["English".localized(), "Spanish".localized(), "Portuguese".localized() ] {
                if txtLangauge.text == strTitle {
                    actionSheet.addAction(UIAlertAction(title: strTitle, style: .destructive, handler: { action in
                        self.dismiss(animated: true) {
                        }
                    }))
                } else {
                    actionSheet.addAction(UIAlertAction(title: strTitle, style: .default, handler: { action in
                        if (strTitle == "English".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_english"
                            ])
                            Localize.setCurrentLanguage("en")
                            UserdefaultManager().setPreference("en" as AnyObject, strKey: APPLANGUAGE)
                        } else if (strTitle == "Spanish".localized()) {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                                           AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? ""))_spanish"
                                                       ])
                            Localize.setCurrentLanguage("es")
                            UserdefaultManager().setPreference("es" as AnyObject, strKey: APPLANGUAGE)
                        } else {
                            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? ""))_portuguese"
                            ])
                            Localize.setCurrentLanguage("pt")
                            UserdefaultManager().setPreference("pt" as AnyObject, strKey: APPLANGUAGE)
                        }
                        self.localizationSetup()
                    }))
                }
        }
        if UI_USER_INTERFACE_IDIOM() == .pad {
            actionSheet.modalPresentationStyle = .popover
            let popPresenter: UIPopoverPresentationController? = actionSheet.popoverPresentationController
            popPresenter?.sourceView =  self.view;
            popPresenter?.sourceRect =  self.view.bounds;
        }
        
        present(actionSheet, animated: true)
    }
    
}


extension ProfileViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == txtLangauge {
            textField.resignFirstResponder()
            self.actionLanguageSheet()
        }
         /*if textField == txtMaptype {
            textField.resignFirstResponder()
            self.actionMapSheet()
        }*/
    }
}
