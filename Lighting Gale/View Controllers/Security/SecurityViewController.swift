//
//  ViewController.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAnalytics
import SkyFloatingLabelTextField

class SecurityViewController: LGParent {
    var clientID                : String!
    @IBOutlet var lblSecurity   : UILabel!
    @IBOutlet var btnNext       : UIButton!
    @IBOutlet var txtSecurity   : UITextField!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    //MARK:- viewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        setupLocalization()
        txtSecurity.text = (UserdefaultManager().getPreferenceForkey(SECURITY) as? String)
    }
    
    func setupLocalization() {
        lblSecurity.text            = "Security Code".localized()
        btnNext.setTitle("Next".localized(), for: .normal)
        txtSecurity.placeholder     = "Enter Security Code".localized()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       Analytics.setScreenName("securityCode", screenClass: "securityCode")
       /*Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
        AnalyticsParameterItemID: "id IOS",
        AnalyticsParameterItemName: "TEST IOS",
        AnalyticsParameterContentType: "cont IOS"
       ])*/
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let loginVC : ViewController = segue.destination as! ViewController
        loginVC.clientID = clientID
        loginVC.securityCode = (sender as! String)
    }
    
    
    //MARK:- forgotPWD
    @IBAction func btnSubmit_Click(sender : UIButton) {
            
        txtSecurity.resignFirstResponder()
        if txtSecurity.text != "" {
            var dictData : [String : AnyObject] = [:]
            dictData["source"]  = "IOS" as AnyObject
            dictData["code"]    = txtSecurity.text as AnyObject
            
            ProfileServices().security(parameters: dictData, headerParams: [:], showLoader: true) { (responseData, isSuccess) in
                if isSuccess {
                    self.clientID = (responseData["ClientID"] as! String)
                    appDelegate.baseUrlDev = (responseData["APIURL"] as! String)
                    appDelegate.isFrmPaid  = (responseData["apptype"] as! String)
                    appDelegate.strMsgFrmAPI  = (responseData["apptypemessage"] as? String)
                    appDelegate.strNearMe  = (responseData["nearmedistance"] as? String)
                    appDelegate.strNearMsg = (responseData["nearmedistancemessage"] as? String)
                    appDelegate.strClientNm = (responseData["ClientName"] as? String)
                    
                    Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                        AnalyticsParameterItemName: appDelegate.strClientNm+"_"+"securityCode"
                    ])
                    
                    self.performSegue(withIdentifier: "loginVC", sender: self.txtSecurity.text)
                    self.txtSecurity.text = ""
                }
            }
        } else {
            AlertView().showAlert(strMsg: "Please enter security code".localized(), btntext: "OK".localized()) { (str) in
            }
        }
    }
}

extension SecurityViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
