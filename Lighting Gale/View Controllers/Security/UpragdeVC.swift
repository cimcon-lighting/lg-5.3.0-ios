//
//  UpragdeVC.swift
//  Lighting Gale
//
//  Created by Akash on 12/03/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import FirebaseAnalytics

class UpragdeVC: LGParent {
    
    @IBOutlet weak var lblMsg : UILabel!
    
    @IBOutlet weak var btnContact : UIButton!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Analytics.setScreenName("upgradeVC", screenClass: "UpragdeVC")
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        lblMsg.text = appDelegate.strMsgFrmAPI.localized()
        btnContact.setTitle("CONTACT ME".localized(), for: .normal)
    }
    
    @IBAction func btnClose_Click(sender : UIButton) {
        self.dismiss(animated: true) {
        }
    }
    
    @IBAction func btnContactMe_Click(sender : UIButton) {
        
        var dictData : [String : AnyObject] = [:]
        dictData["Email"]       = serviceHandler.objUserModel.strUName    as AnyObject
        dictData["Name"]        = serviceHandler.objUserModel.strFName    as AnyObject
        dictData["Clientid"]    = serviceHandler.objUserModel.strClientId as AnyObject
        
        ProfileServices().upgradeClient(parameters: dictData, headerParams: [:], showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                    AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_contactMe"
                ])
                AlertView().showAlert(strMsg: (responseData[MSG] as! String), btntext: "OK".localized()) { (str) in
                    self.dismiss(animated: true) {
                    }
                }
            }
        }
        
        /*let subject          = "Upgrade to Paid version"
         let body            = ""
         let encodedParams   = "subject=\(subject)&body=\(body)".addingPercentEncoding(withAllowedCharacters: .urlPathAllowed)
         
         if let url = URL(string: "mailto:Mobile.support@cimconlighting.com?\(encodedParams!)") {
         if #available(iOS 10.0, *) {
         UIApplication.shared.open(url)
         } else {
         UIApplication.shared.openURL(url)
         }
         }*/
    }
}
