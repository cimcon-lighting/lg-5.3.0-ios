//
//  SentCommandVC.swift
//  Lighting Gale
//
//  Created by Apple on 07/03/19.
//  Copyright © 2019 Apple. All rights reserved.
//

import UIKit

import FirebaseAnalytics

class CellSentCommands: UITableViewCell {
    @IBOutlet var lblTrackID            : UILabel?
    @IBOutlet var lblCommand            : UILabel?
    @IBOutlet var lblStatus             : UILabel?
    @IBOutlet var lblSentBy             : UILabel?
    @IBOutlet var lblTime               : UILabel?
    @IBOutlet var lblDesc               : UILabel?
    @IBOutlet var lblGatewayNm          : UILabel?
    
    @IBOutlet var lblHeadTrack          : UILabel?
    @IBOutlet var lblHeadTime           : UILabel?
    @IBOutlet var lblHeadCommand        : UILabel?
    @IBOutlet var lblHeadStatus         : UILabel?
}

class SentCommandVC: LGParent {
    @IBOutlet var txtCmdNm          : UITextField!
    @IBOutlet var txtStatus         : UITextField!
    @IBOutlet var txtTimeFrom       : UITextField!
    @IBOutlet var txtTimeTo         : UITextField!
    @IBOutlet var txtGateWayNm      : UITextField!
    @IBOutlet var txtTrackID        : UITextField!
    
    @IBOutlet var lblMsg            : UILabel!
    
    @IBOutlet var tblView       : UITableView!
    
    @IBOutlet var vwTop         : UIView!
    
    @IBOutlet var constVWHeight : NSLayoutConstraint!
    
    var isFilterOpen            : Bool! = false
    
    var arrTblData              : [AnyObject]!
    var arrGateway              : [AnyObject]!
    var arrSearchData           : [String]!
    
    let datePicker              = UIDatePicker()
    
    var isFrmDtPicker           : Bool!
    
    var totalCount              : Int! = 1
    var pageCount               : Int! = 1
    var selectedTAG             : Int! = 1
    
    var refreshControl          : UIRefreshControl!
    
    var strStatusID             : String!
    var strCmdID                : String!
    
    @IBOutlet var btnSearch     : UIButton!
    @IBOutlet var btnClear      : UIButton!
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tblView.addSubview(refreshControl)
        
        
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 300
        
        lblMsg.isHidden = true
        
        constVWHeight.constant = 0
        
        if let arr = UserDefault.value(forKey: "CLIENTTYPE") {
            let arrClientType = arr as! [[String : AnyObject]]
            
            let filterdArray = arrClientType.filter { ($0["clientType"] as! String) == ("Zigbee") }
            if filterdArray.count > 0 {
                txtGateWayNm.isHidden = false
                /*txtGateway.isHidden = false
                lblPrompt.text = "The values reflect the most recent polls conducted by the individual gateways.".localized()*/
            }
        }
        
        /*if self.serviceHandler.objUserModel.strClienttype == "1" {
            txtGateWayNm.isHidden = false
        }*/
        
        
        tblView.rowHeight = UITableView.automaticDimension
        
        self.addRightBarButton(imgRightbarButton: "Filter")
        
        self.addLeftBarButton(imgLeftbarButton: "back")
        arrTblData = []
        tblView.tableFooterView = UIView()
        
    }
    
    
    //AMRK:- ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
            
        self.navigationItem.title = "Sent Commands".localized()
        
        txtTimeFrom.placeholder = "Send Date".localized()
        
        btnSearch.setTitle("SEARCH".localized(), for: .normal)
        btnClear.setTitle("CLEAR".localized(), for: .normal)
        
        let selectLocalize = "Select".localized()
                
        txtCmdNm.placeholder = "Command".localized()
        txtTrackID.placeholder = "Track ID".localized()
        txtGateWayNm.placeholder = "Gateway".localized()
        txtStatus.placeholder = "Status".localized()
        
        if txtCmdNm.text == "Select" || txtCmdNm.text == "Selecionar" || txtCmdNm.text == "Seleccionar" {
            txtCmdNm.text         = selectLocalize
        }
        
        if txtStatus.text == "Select" || txtStatus.text == "Selecionar" || txtStatus.text == "Seleccionar" {
            txtStatus.text         = selectLocalize
        }
        
        if txtTrackID.text == "Select" || txtTrackID.text == "Selecionar" || txtTrackID.text == "Seleccionar" {
            txtTrackID.text         = selectLocalize
        }
       
        if txtGateWayNm.text == "Select" || txtGateWayNm.text == "Selecionar" || txtGateWayNm.text == "Seleccionar" {
            txtGateWayNm.text         = selectLocalize
        }
        
        
        if txtTimeFrom.text == "From Date" || txtTimeFrom.text == "la fecha inicial" || txtTimeFrom.text == "De Data" {
            txtTimeFrom.text         = "From Date".localized()
        }
        
        if txtTimeTo.text == "To Date" || txtTimeTo.text == "la fecha final" || txtTimeTo.text == "Até Data" {
            txtTimeTo.text         = "To Date".localized()
        }
        
        getDataFrmServer(isLoadMore: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        Analytics.setScreenName("sentCommand", screenClass: "sentCommand")
    }
    
    /*override func leftButtonClick() {
     
     }*/
    @objc func refresh(sender:AnyObject) {
        clearBtn_Click(sender: UIButton.init())
    }
    override func rightButtonClick() {
        if !isFilterOpen {
            isFilterOpen = true
            constVWHeight .constant = 192
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        } else {
            isFilterOpen = false
            constVWHeight .constant = 0
            UIView.animate(withDuration: 0.15) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    //MARK:- isValid
    func isValid()-> Bool {
        if txtTimeFrom.text == "From Date".localized() && txtTimeTo.text == "To Date".localized() {
            return true
        } else if txtTimeFrom.text != "" && txtTimeTo.text == "To Date".localized() {
            AlertView().showAlert(strMsg: "Please choose To date".localized(), btntext: "OK".localized()) { (str) in
            }
            return false
        } else if txtTimeTo.text != "" && txtTimeFrom.text == "From Date".localized() {
            AlertView().showAlert(strMsg: "Please choose From date".localized(), btntext: "OK".localized()) { (str) in
            }
            return false
        } else if txtTimeFrom.text != "" && txtTimeTo.text != "" {
            let formatter = DateFormatter()
            formatter.dateFormat = self.serviceHandler.objUserModel.strDtFrmat
            let startDt = formatter.date(from: txtTimeFrom.text!)
            let endDate = formatter.date(from: txtTimeTo.text!)
            if endDate!.timeIntervalSince(startDt!).sign == FloatingPointSign.minus {
                AlertView().showAlert(strMsg: "From date should not be greater than To date".localized(), btntext: "OK".localized()) { (str) in
                }
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }
    
    
    func showDatePicker() {
        //Formate Date
        datePicker.datePickerMode = .date
        datePicker.maximumDate = NSDate.init() as Date
        datePicker.date = Date()
        
        /*let formatter = DateFormatter()
         formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         
         let myString = formatter.string(from: Date())
         let yourDate = formatter.date(from: myString)
         
         print(self.serviceHandler.objUserModel.strDtFrmat)
         
         let formatter1 = DateFormatter()
         formatter1.dateFormat = self.serviceHandler.objUserModel.strDtFrmat
         let myFString = formatter1.string(from: yourDate!)
         let yourFDate = formatter1.date(from: myFString)
         
         
         print(myFString)
         print(yourFDate)
         
         datePicker.date = yourFDate!*/
        
        /*let dateStringFormatter = DateFormatter()
         dateStringFormatter.dateFormat = self.serviceHandler.objUserModel.strDtFrmat!
         dateStringFormatter.timeZone = TimeZone(abbreviation: "UTC")
         
         let defaultDateStringFormatter = DateFormatter()
         defaultDateStringFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss Z"
         defaultDateStringFormatter.timeZone = TimeZone(abbreviation: "UTC")
         
         let convertedStartDate : Date = defaultDateStringFormatter.date(from: (String(describing:Date.init())))!
         print(dateStringFormatter.date(from: (String(describing:convertedStartDate))))
         
         let newConverted    = dateStringFormatter.date(from: (String(describing:Date.init())))*/
        
        
        //ToolBar
        let toolbar = UIToolbar();
        toolbar.sizeToFit()
        let doneButton      = UIBarButtonItem(title: "Done".localized(), style: .plain, target: self, action: #selector(donedatePicker));
        let spaceButton     = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton    = UIBarButtonItem(title: "Cancel".localized(), style: .plain, target: self, action: #selector(cancelDatePicker));
        
        toolbar.setItems([doneButton,spaceButton,cancelButton], animated: false)
        
        txtTimeFrom.inputAccessoryView      = toolbar
        txtTimeFrom.inputView               = datePicker
        
        txtTimeTo.inputAccessoryView        = toolbar
        txtTimeTo.inputView                 = datePicker
    }
    
    @objc func donedatePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = self.serviceHandler.objUserModel.strDtFrmat
        if isFrmDtPicker {
            txtTimeFrom.text = formatter.string(from: datePicker.date)
        } else {
            txtTimeTo.text = formatter.string(from: datePicker.date)
        }
        self.view.endEditing(true)
    }
    
    @objc func cancelDatePicker(){
        self.view.endEditing(true)
    }
    
    @IBAction func searchBtnClick (sender : UIButton) {
        if isValid() {
            Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
                AnalyticsParameterItemName: "\(String(describing: appDelegate.strClientNm ?? ""))_\(self.serviceHandler.objUserModel.strUName ?? "")_searchSentCommand"
            ])
            
            pageCount   = 1
            totalCount  = 1
            arrTblData.removeAll()
            tblView.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.getDataFrmServer(isLoadMore: true)
            })
        }
    }
    
    @IBAction func clearBtn_Click(sender : UIButton) {
        pageCount   = 1
        totalCount  = 1
        arrTblData.removeAll()
        
        txtCmdNm.text       = "Select".localized()
        txtStatus.text      = "Select".localized()
        txtTimeFrom.text    = "From Date".localized()
        txtTimeTo.text      = "To Date".localized()
        txtGateWayNm.text   = "Select".localized()
        txtTrackID.text     = "Select".localized()
        
        tblView.reloadData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
            self.getDataFrmServer(isLoadMore: true)
        })
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DetailIdentifier" {
            let detailsVC : DetailSentViewController = segue.destination as! DetailSentViewController
            detailsVC.dictTradkDetails = (sender as! [String : AnyObject])
        } else if segue.identifier == "SearchIdenifier" {
            let searchVC : SearchViewController = segue.destination as! SearchViewController
            searchVC.searchDelegate = self
            searchVC.strPlasHolder  = (sender as! String)
            searchVC.arrTblData     = arrSearchData
            searchVC.currentTag     = selectedTAG
            searchVC.arrMainData    = arrGateway
        }
    }
    
    
    //MARK:- GET DATA FRM SERVE
    func getDataFrmServer(isLoadMore : Bool) {
        
        if arrTblData.count != totalCount {
            
            let strSelectLoc = "Select".localized()
            
            var dictHeader : [String : String] = [:]
            dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
            dictHeader[CONTENT_TYPE]    = "application/json"
            dictHeader[PAGE_START_INDEX] = String(pageCount)
            dictHeader[PAGE_SIZE]       = "10"
            
            var dictParams : [String : AnyObject] = [:]
            
            let startDt  = "From Date".localized()
            let toDt     = "To Date".localized()
            
            var dictTmp : [String : String] = [:]
            dictTmp["SLCID"]        = ""
            dictTmp["Gateway"]      = txtGateWayNm.text == strSelectLoc ? "" : txtGateWayNm.text
            dictTmp["CommandName"]  = txtCmdNm.text     == strSelectLoc ? "" : strCmdID
            dictTmp["Status"]       = txtStatus.text    == strSelectLoc ? "" : strStatusID
            dictTmp["SentBy"]       = ""//txt.text == "Select" ? "" : txtTrackID.text
            dictTmp["TrackId"]      = txtTrackID.text   == strSelectLoc   ? "" : txtTrackID.text
            dictTmp["FromDate"]     = txtTimeFrom.text  == startDt ? "" : txtTimeFrom.text
            dictTmp["ToDate"]       = txtTimeTo.text    == toDt ? "" : txtTimeTo.text
            
            let arrTmp : [AnyObject]        = [dictTmp as AnyObject]
            dictParams[SENT_COMMAND_OBJ]    = arrTmp as AnyObject
            
            ProfileServices().sentCommandList(parameters: dictParams, headerParams: dictHeader, showLoader: isLoadMore) { (responseData, isSuccess) in
                if isSuccess {
                    let arrTmp : [AnyObject] = responseData[TRACK_LIST] as! [AnyObject]
                    self.arrTblData.append(contentsOf: arrTmp)
                    self.totalCount = Int(responseData[TOTAL_RECORDS] as! String)
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.pageCount += 1
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                } else {
                    if self.arrTblData.count == 0 {
                        self.lblMsg.text = "No data Found".localized()
                        self.lblMsg.isHidden = false
                    } else {
                        self.lblMsg.isHidden = true
                    }
                    self.refreshControl.endRefreshing()
                    self.tblView.reloadData()
                    self.totalCount = self.arrTblData.count
                }
            }
        } else {
            self.refreshControl.endRefreshing()
            if self.arrTblData.count == 0 {
                self.lblMsg.text = "No data Found".localized()
                self.lblMsg.isHidden = false
            }/* else {
             self.lblMsg.isHidden = true
             }*/
            //self.tblView.reloadData()
        }
        
    }
    
    //MARK:- getTrackID
    func getTrackID() {
        
        self.performSegue(withIdentifier: "SearchIdenifier", sender: "Track ID".localized())
        
        /*var dictHeader : [String : String] = [:]
         dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
         dictHeader[CONTENT_TYPE]    = "application/json"
         
         ProfileServices().commandTrackID(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
         if isSuccess {
         self.arrSearchData = self.getvalueFrmDict(dict : responseData, isFrm: "6")
         print(self.arrSearchData)
         self.performSegue(withIdentifier: "SearchIdenifier", sender: "Track ID")
         }
         }*/
    }
    
    
    //MARK:- getWayList
    func getWayList() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getCommandGatwayList(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                self.arrGateway = responseData[DATA] as! [AnyObject]
                self.arrSearchData = self.getvalueFrmDict(dict : responseData, isFrm: "5")
                print(self.arrSearchData)
                self.performSegue(withIdentifier: "SearchIdenifier", sender: "Gateway")
            }
        }
    }
    
    
    //MARK:- getCommandStatus
    func getCommandStatus() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getCommandStatus(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                
                self.arrGateway = responseData[DATA] as! [AnyObject]
                self.arrSearchData = self.getvalueFrmDict(dict : responseData, isFrm: "2")
                self.performSegue(withIdentifier: "SearchIdenifier", sender: "Status".localized())
            }
        }
    }
    
    
    //MARK:- getCommandStatus
    func getCommandClientType() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getCommandClientType(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
            }
        }
    }
    
    
    //MARK:- getCommand
    func getCommand() {
        var dictHeader : [String : String] = [:]
        dictHeader[AUTHORIZATION]   = serviceHandler.objUserModel.strTokenType!+" "+serviceHandler.objUserModel.strAccessToken!
        dictHeader[CONTENT_TYPE]    = "application/json"
        
        ProfileServices().getCommand(headerParams: dictHeader, showLoader: true) { (responseData, isSuccess) in
            if isSuccess {
                self.arrGateway = responseData[DATA] as! [AnyObject]
                self.arrSearchData = self.getvalueFrmDict(dict : responseData, isFrm: "1")
                print(self.arrSearchData)
                self.performSegue(withIdentifier: "SearchIdenifier", sender: "Command".localized())
            }
        }
    }
    
    func getvalueFrmDict(dict : [String : AnyObject], isFrm : String) -> [String] {
        var arrTmp : [String] = []
        if isFrm == "1" {
            for dict in dict[DATA] as! [AnyObject] {
                arrTmp.append(dict["commandName"] as! String)
            }
        } else  if isFrm == "2" {
            for dict in dict[DATA] as! [AnyObject] {
                arrTmp.append(dict["key"] as! String)
            }
        } else if isFrm == "3" {
            
        } else if isFrm == "4" {
            
        }  else if isFrm == "5" {
            for dict in dict[DATA] as! [AnyObject] {
                arrTmp.append(dict["gatewayName"] as! String)
            }
        } else if isFrm == "6" {
            for dict in dict[DATA] as! [AnyObject] {
                arrTmp.append(dict as! String)
            }
        }
        return arrTmp
    }
    
}
extension SentCommandVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrTblData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : CellSentCommands = tableView.dequeueReusableCell(withIdentifier: "CellSentCommands") as! CellSentCommands
        
        cell.lblHeadTime?.text      = "Time".localized()
        cell.lblHeadStatus?.text    = "Status".localized()
        cell.lblHeadTrack?.text     = "Track ID".localized()
        cell.lblHeadCommand?.text   = "Command".localized()
        
        if indexPath.row % 2 != 0 {
            cell.backgroundColor = UIColor.init(red: 232/255, green: 232/255, blue: 232/255, alpha: 1.0)
        } else {
            cell.backgroundColor = UIColor.white
        }
        
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
        
        cell.lblTime!.text       = dictData[TIME]            as? String
        cell.lblCommand!.text    = dictData[CMD_NM]          as? String
        cell.lblStatus!.text     = dictData["statusDetail"]  as? String
        //cell.lblSentBy.text     = dictData[SENT_BY]         as? String
        //cell.lblGatewayNm!.text  = dictData["gatewayName"]  as? String
        cell.lblTrackID!.text    = dictData[TRACK_ID]        as? String
        
        if indexPath.row == self.arrTblData.count-1 {
            getDataFrmServer(isLoadMore: false)
        }
        
        return cell
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dictData : [String : AnyObject] = arrTblData[indexPath.row] as! [String : AnyObject]
        if dictData["sendstatus"] as! String != "3" {
            DispatchQueue.main.async {
                self.performSegue(withIdentifier: "DetailIdentifier", sender: dictData)
            }
        }
    }
    
    private func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension SentCommandVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        txtCmdNm.resignFirstResponder()
        txtStatus.resignFirstResponder()
        txtTrackID.resignFirstResponder()
        txtGateWayNm.resignFirstResponder()
        if textField == txtTimeTo || textField == txtTimeFrom {
            if textField == txtTimeFrom{
                isFrmDtPicker = true
            } else {
                isFrmDtPicker = false
            }
            showDatePicker()
        } else {
            //self.performSegue(withIdentifier: "SearchIdenifier", sender: textField.placeholder)
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        selectedTAG = textField.tag
        if textField == txtTrackID {
            getTrackID()
        } else if textField == txtCmdNm {
            getCommand()
        } else if textField == txtStatus {
            getCommandStatus()
        } else if textField == txtGateWayNm {
            getWayList()
        }
        return true
    }
}


extension SentCommandVC : SearchVCDelegate {
    func changeSearchParams(_ placeHolder: String, tagTextField: Int) {
        
    }
    
    func changeSearchParamsStatus(_ placeHolder: String, tagTextField: Int ,strGroupID: String) {
        if tagTextField == 1 {
            strCmdID      = strGroupID
            txtCmdNm.text = placeHolder
        } else if tagTextField == 2 {
            strStatusID = strGroupID
            txtStatus.text = placeHolder
        } else if tagTextField == 5 {
            txtGateWayNm.text = placeHolder
        } else if tagTextField == 6 {
            txtTrackID.text = placeHolder
        }
    }
}
