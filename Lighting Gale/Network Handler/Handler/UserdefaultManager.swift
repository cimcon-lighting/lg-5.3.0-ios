//
//  UserdefaultManager.swift
//  RealPlans
//
//  Created by Bhuvan on 19/01/16.
//  Copyright © 2016 OpenXcell Studio. All rights reserved.
//

import Foundation

open class UserdefaultManager {
    
    func setUserModel(_ user: UserModel) {
        if UserModel?(user) != nil {
            let encodeObject: Data = NSKeyedArchiver.archivedData(withRootObject: user)
            UserDefault.set(encodeObject, forKey: USERMODEL)
        }
        else {
            UserDefault.removeObject(forKey: USERMODEL)
        }
    }
    
    func getUserModel() -> AnyObject? {
        if ((UserDefault.object(forKey: USERMODEL)) != nil) {
            let encodeObject: Data = (UserDefault.object(forKey: USERMODEL) as? Data)!
            return NSKeyedUnarchiver.unarchiveObject(with: encodeObject)! as! UserModel
        }
        return nil
    }
    
    
    // MARK: Clear Sessions
      func clearSessions() {
        ServiceHandler.sharedInstance().objUserModel = UserModel.init()
        UserDefault.removeObject(forKey: USERMODEL)
     }
    
    //MARK: - Save to User Defaults
    func getPreferenceForkey(_ strKey: String) -> AnyObject? {
        if UserDefaults.standard.object(forKey: strKey) != nil {
            let value : AnyObject? = UserDefaults.standard.object(forKey: strKey)! as AnyObject?
            return value!
        } else {
            return nil
        }
    }
    
    func setPreference(_ value: AnyObject, strKey: String) {
        UserDefaults.standard.set(value, forKey: strKey)
        UserDefaults.standard.synchronize()
    }
    
}
